# Aviasales & Kozel

`npm i` - старт проекта.

`bower i package` - установка плагинов. Далее `js` файлы нужно подключить в `gulpfile.js` в таске `libs`.

`gulp` - запуск сервера, автокомпиляция sass и т.д.

`gulp build` - сборка проекта.

`gulp deploy` - upload проекта по ftp.

`gulp clearcache` - очистка кэша.
